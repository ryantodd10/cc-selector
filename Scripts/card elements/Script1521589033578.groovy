import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://qantas-credit-card-selector.surge.sh/')

WebUI.maximizeWindow()

WebUI.verifyElementPresent(findTestObject('Page_React App/card title'), 0)

WebUI.delay(10)

WebUI.verifyElementPresent(findTestObject('Page_React App/Card image'), 0)

WebUI.verifyElementPresent(findTestObject('Page_React App/card type'), 0)

WebUI.verifyElementPresent(findTestObject('Page_React App/card perks feature 1'), 0)

WebUI.verifyElementPresent(findTestObject('Page_React App/card perks feature 2'), 0)

WebUI.verifyElementPresent(findTestObject('Page_React App/card perks feature 3'), 0)

WebUI.verifyElementPresent(findTestObject('Page_React App/a_FIND OUT MORE'), 0)

WebUI.verifyElementPresent(findTestObject('Page_React App/Annual fee'), 0)

WebUI.verifyElementPresent(findTestObject('Page_React App/div_TERMS  CONDITIONS'), 0)

WebUI.click(findTestObject('Page_React App/Card image'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementPresent(findTestObject('Page_React App/Page_React App/div_Interest Rate'), 0)

WebUI.verifyElementPresent(findTestObject('Page_React App/Page_React App/div_19.99 p.a. on eligible pur'), 0)

WebUI.verifyElementPresent(findTestObject('Page_React App/Page_React App/div_Capping'), 0)

WebUI.verifyElementPresent(findTestObject('Page_React App/Page_React App/div_Uncapped Qantas Points ear'), 0)

WebUI.verifyElementPresent(findTestObject('Page_React App/Page_React App/div_Travel Benefits'), 0)

WebUI.verifyElementPresent(findTestObject('Page_React App/Page_React App/li_0 currency conversion'), 0)

WebUI.verifyElementPresent(findTestObject('Page_React App/Page_React App/div_content___15zph'), 0)

WebUI.closeBrowser()

